import math
import random
from functools import reduce
import itertools
from PIL import Image, ImageDraw

# Operador estàtic que pinta una llista de polígons en una imatge de 400x400
# Rep: Una llista de polígons i un paràmetre opcional que indica el filename on guardar la imatge
# Resultat: Es guarda una imatge amb el filename indicat on estan pintats els polígons de la llista polygons
def draw_convex_polygons(polygons, filename="output.png"):
    def transform_coordinate(point):
        # NewValue = (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin
        new_point = ((point[0] - min_coord) * 399 / (max_coord - min_coord),
                     (399 - (399 * (point[1] - min_coord) / (max_coord - min_coord))))
        return new_point

    def transform_polygon(poly):
        new_poly = ConvexPolygon([transform_coordinate(p) for p in poly.get_vertices()])
        new_poly.set_color(poly.get_color())
        return new_poly

    img = Image.new('RGB', (400, 400), 'White')
    dib = ImageDraw.Draw(img)
    if len(polygons) > 1:
        hull = reduce(lambda acc, p: acc.convex_union(p), polygons)
    else:
        hull = polygons[0]
    bounding_box = hull.get_bounding_box()
    min_x = bounding_box.get_vertices()[1][0]
    max_y = bounding_box.get_vertices()[1][1]
    max_x = bounding_box.get_vertices()[3][0]
    min_y = bounding_box.get_vertices()[3][1]
    max_coord = max(max_x, max_y)
    min_coord = min(min_x, min_y)
    new_polygons = [transform_polygon(poly) for poly in polygons]
    for poly in new_polygons:
        dib.polygon(poly.get_vertices(), 'White', poly.get_color())

    img.save(filename)


class ConvexPolygon:

    # Constructora de la classe ConvexPolygon
    # Rep: Una llista de punts, i un booleà que indica si s'està construïnt una bounding box o no
    # Resultat: Es defineix un polígon amb els punts de points, i es calculen totes les propietats del polígon
    def __init__(self, points, box=False):
        if len(points) > 2:
            self.points = self.__order_points_clockwise(points)
            self.segments = self.__get_pairs_of_points()
            self.perimeter = self.__calc_perimeter()
            self.signed_area = self.__calc_signed_area()
            self.regular = self.__calc_is_regular()
            self.centroid = self.__calc_centroid()
            if not box:
                self.bounding_box = ConvexPolygon(self.__calc_bounding_box(), True)
            else:
                self.bounding_box = None
            self.color = (0, 0, 0)
        elif len(points) == 2:
            self.points = self.__order_points_clockwise(points)
            self.segments = self.__get_pairs_of_points()
            self.perimeter = self.__calc_perimeter()
            self.signed_area = 0
            self.regular = True
            self.centroid = None
            self.bounding_box = None
            self.color = (0, 0, 0)
        else:
            self.points = points
            self.segments = []
            self.perimeter = 0
            self.signed_area = 0
            self.regular = True
            self.centroid = None
            self.bounding_box = None
            self.color = (0, 0, 0)

    # Operador privat que ordena punts en sentit horari
    # Rep: Una llista de punts
    # Resultat: Una llista de punts ordenada en sentit horari, començant pel punt que està més a l'esquerra
    # i, en cas d'empat, més abaix
    def __order_points_clockwise(self, points):
        def angle_and_distance(ori, point):
            v = [point[0] - ori[0], point[1] - ori[1]]
            length_v = math.hypot(v[0], v[1])
            if length_v == 0:
                return -math.pi, 0
            n = [v[0] / length_v, v[1] / length_v]
            prod1 = n[0] * 0 + n[1] * 1
            prod2 = 1 * n[0] + 0 * n[1]
            angle = math.atan2(prod2, prod1)
            if angle < 0:
                return 2 * math.pi + angle, length_v
            return angle, length_v

        leftmost_point = min(points, key=lambda p: (p[0], p[1]))
        points = [p for p in points if p != leftmost_point]
        points = sorted(points, key=lambda p: angle_and_distance(leftmost_point, p))
        points = [leftmost_point] + points
        return points

    # Operador privat que calcula tots els segments d'un polígon
    # Rep: El paràmetre implícit self
    # Retorna: Totes les parelles de punts consecutius del polígon self
    def __get_pairs_of_points(self):
        n_verts = self.get_num_edges_and_verts()
        return [(self.points[i], self.points[(i + 1) % n_verts]) for i in range(n_verts)]

    # Operador privat que calcula el perímetre d'un polígon
    # Rep: El paràmetre implícit self
    # Retorna: El perímetre de self
    def __calc_perimeter(self):
        lengths = map(lambda p: math.hypot(p[1][0] - p[0][0], p[1][1] - p[0][1]), self.segments)
        return reduce(lambda acc, y: acc + y, lengths)

    # Operador privat que calcula l'àrea d'un polígon
    # Rep: El paràmetre implícit self
    # Retorna: L'àrea de self amb signe (calculada amb la fòrmula del "Shoelace")
    def __calc_signed_area(self):
        a = map(lambda pair: pair[0][0] * pair[1][1] - pair[1][0] * pair[0][1], self.segments)
        return reduce(lambda acc, y: acc + y, a) / 2

    # Operador privat que calcula el centroide d'un polígon
    # Rep: El paràmetre implícit self
    # Retorna: El centroide de self
    def __calc_centroid(self):
        elems_x = map(lambda p: (p[0][0] + p[1][0]) * (p[0][0] * p[1][1] - p[1][0] * p[0][1]), self.segments)
        elems_y = map(lambda p: (p[0][1] + p[1][1]) * (p[0][0] * p[1][1] - p[1][0] * p[0][1]), self.segments)
        c_x, c_y = reduce(lambda acc, y: acc + y, elems_x) / (6 * self.get_area()), reduce(lambda acc, y: acc + y,
                                                                                           elems_y) / (
                           6 * self.get_area())
        return [c_x, c_y]

    # Operador privat que calcula la "bounding box" d'un polígon
    # Rep: El paràmetre implícit self
    # Retorna: La "bounding box" de self en forma de llista de punts
    def __calc_bounding_box(self):
        verts = self.get_vertices()
        top = max(verts, key=lambda p: p[1])[1]
        bottom = min(verts, key=lambda p: p[1])[1]
        left = min(verts, key=lambda p: p[0])[0]
        right = max(verts, key=lambda p: p[0])[0]
        return [[left, top], [right, top], [right, bottom], [left, bottom]]

    # Operador privat que calcula si un polígon és regular
    # Rep: El paràmetre implícit self
    # Retorna: Un booleà que indica si self és regular
    def __calc_is_regular(self):
        return self.is_equilateral() and self.is_equiangular()

    # Operador que calcula si un punt es troba dins un polígon
    # Rep: El paràmetre implícit self i un punt
    # Retorna: Un booleà que indica si point es troba dins de self
    def point_inside_polygon(self, point):
        def side_of_segment(_v1, _v2):
            x = _v1[0] * _v2[1] - _v1[1] * _v2[0]
            if x == 0:
                return None
            elif x < 0:
                return "l"
            else:
                return "r"

        prev_side = None
        for pair in self.segments:
            p1, p2 = pair[0], pair[1]
            v1 = (p1[0] - p2[0], p1[1] - p2[1])
            v2 = (p1[0] - point[0], p1[1] - point[1])
            curr_side = side_of_segment(v1, v2)
            if not curr_side is None:
                if prev_side is None:
                    prev_side = curr_side
                elif prev_side != curr_side:
                    return False
        return True

    # Operador que calcula si un polígon es troba dins un altre polígon
    # Rep: El paràmetre implícit self i un polígon
    # Retorna: Un booleà que indica si poly es troba dins self
    def poly_inside_polygon(self, poly):
        vert = poly.get_vertices()
        inside = map(lambda p: self.point_inside_polygon(p), vert)
        return reduce(lambda acc, pred: acc and pred, inside, True)

    # Getters i setters

    def get_num_edges_and_verts(self):
        return len(self.points)

    def get_vertices(self):
        return self.points

    def get_perimeter(self):
        return self.perimeter

    def get_signed_area(self):
        return self.signed_area

    def get_area(self):
        return abs(self.signed_area)

    def get_centroid(self):
        return self.centroid

    def get_color(self):
        return self.color

    def set_color(self, color):
        self.color = tuple(c for c in color)

    def is_regular(self):
        return self.regular

    def get_bounding_box(self):
        return self.bounding_box

    # Operador que calcula si un polígon és equilateral
    # Rep: El paràmetre implícit self
    # Resultat: Un booleà que indica si self és equilateral
    def is_equilateral(self):
        lengths = [math.hypot(s[1][0] - s[0][0], s[1][1] - s[0][1]) for s in self.segments]
        equal = [length == lengths[0] for length in lengths]
        return reduce(lambda acc, pred: acc and pred, equal, True)

    # Operador que calcula si dos polígons són iguals
    # Rep: El paràmetre implícit self i un polígon
    # Resultat: Un booleà que indica si self i poly2 són iguals
    def equals(self, poly2):
        return self.points == poly2.get_vertices()

    # Operador que calcula si un polígon és equiangular
    # Rep: El paràmetre implícit self
    # Resultat: Un booleà que indica si self és equiangular
    def is_equiangular(self):
        vectors = [[s[1][0] - s[0][0], s[1][1] - s[0][1]] for s in self.segments]
        vec_pairs = [(vectors[i], vectors[(i + 1) % len(vectors)]) for i in range(len(vectors))]
        angles = [math.acos(
            (p[0][0] * p[1][0] + p[0][1] * p[1][1]) / (math.hypot(p[0][0], p[0][1]) * math.hypot(p[1][0], p[1][1])))
            for p in vec_pairs]
        equal = [angle == angles[0] for angle in angles]
        return reduce(lambda acc, pred: acc and pred, equal, True)

    # Operador que calcula la intersecció entre dos polígons
    # Rep: El paràmetre implícit self i un polígon
    # Resultat: Un polígon resultat de la intersecció entre self i poly2
    # Calculada seguint l'algorisme de clipping de Sutherland-Hodgman
    def intersection(self, poly2):
        def is_right_of(p):
            return (cp2[0] - cp1[0]) * (p[1] - cp1[1]) <= (cp2[1] - cp1[1]) * (p[0] - cp1[0])

        def compute_intersection():
            dc = [cp1[0] - cp2[0], cp1[1] - cp2[1]]
            dp = [s[0] - e[0], s[1] - e[1]]
            n1 = cp1[0] * cp2[1] - cp1[1] * cp2[0]
            n2 = s[0] * e[1] - s[1] * e[0]
            n3 = 1.0 / (dc[0] * dp[1] - dc[1] * dp[0])
            return [(n1 * dp[0] - n2 * dc[0]) * n3, (n1 * dp[1] - n2 * dc[1]) * n3]

        output_list = self.get_vertices()
        cp1 = poly2.get_vertices()[-1]

        for clip_vertex in poly2.get_vertices():
            cp2 = clip_vertex
            input_list = output_list
            output_list = []
            s = input_list[-1]

            for subject_vertex in input_list:
                e = subject_vertex
                if is_right_of(e):
                    if not is_right_of(s):
                        output_list.append(compute_intersection())
                    output_list.append(e)
                elif is_right_of(s):
                    output_list.append(compute_intersection())
                s = e
            cp1 = cp2
        return ConvexPolygon(output_list)

    # Operador que calcula la unió convexa entre dos polígons
    # Rep: El paràmetre implícit self i un polígon
    # Resultat: Un polígon resultat de la unió convexa entre self i poly2
    # Calculada seguint l'algorisme de gift wrapping
    def convex_union(self, poly2):
        def get_convex_hull(_points):
            def is_left_of(p1, p2, p3):
                return (p2[0] - p1[0]) * (p3[1] - p1[1]) > (p2[1] - p1[1]) * (p3[0] - p1[0])

            def find_next_point():
                hull.append(_points[p])
                q = (p + 1) % n
                for i in range(n):
                    if is_left_of(_points[p], _points[q], _points[i]):
                        q = i
                return q

            n = len(points)
            hull = []

            # Pick the leftmost point
            leftmost = min(range(n), key=lambda i: points[i][0])

            p = leftmost
            p = find_next_point()
            while p != leftmost:
                p = find_next_point()
            return ConvexPolygon(hull)

        points = self.get_vertices()
        points += [p for p in poly2.get_vertices() if p not in points]
        return get_convex_hull(points)
