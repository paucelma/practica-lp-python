import sys

sys.path.append("../cl")

from antlr4 import *
from EvalVisitor import EvalVisitor
from PolyParser import PolyParser
from PolyLexer import PolyLexer


def compile_and_execute(filename):
    input_stream = FileStream(filename)
    lexer = PolyLexer(input_stream)
    token_stream = CommonTokenStream(lexer)
    parser = PolyParser(token_stream)

    tree = parser.root()
    visitor = EvalVisitor(telegram=True)
    visitor.visit(tree)
    return visitor.output_list, visitor.image_list
