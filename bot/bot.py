# importa l'API de Telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import CompilerInterface


def execute(update, context):
    input_file = open("script.poly", "w")
    input_file.write(update.message.text)
    input_file.close()
    output_list, image_list = CompilerInterface.compile_and_execute("script.poly")

    output = ''.join(output_list)
    context.bot.send_message(chat_id=update.effective_chat.id, text=output)
    for im in image_list:
        context.bot.send_photo(chat_id=update.effective_chat.id, photo=open(im, 'rb'))


# declara una constant amb el access token que llegeix de token.txt
TOKEN = open('token.txt').read().strip()

# crea objectes per treballar amb Telegram
updater = Updater(token=TOKEN, use_context=True)
dispatcher = updater.dispatcher
execute_handler = MessageHandler(Filters.text & (~Filters.command), execute)
dispatcher.add_handler(execute_handler)

# engega el bot
updater.start_polling()
