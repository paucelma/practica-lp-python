grammar Poly;

root : line+ (inst)? EOF;

line : inst NEWLINE
    | NEWLINE
    ;

inst : 'print' expr # printPoly
    | 'print' '"'anytext'"' #printText
    | 'area' expr # area
    | 'color' expr ',' color # setColor
    | 'perimeter' expr # perimeter
    | 'vertices' expr # vertices
    | 'centroid' expr # centroid
    | 'equal' expr ',' expr # equal
    | 'inside' expr ',' expr # inside
    | 'draw' '"'anytext'"' ',' expr ((',' expr)+)? # draw
    | VAR ':=' expr # assign
    ;

color : '{' NUM NUM NUM '}' ;

expr : '[' numPairSerie ']'  # literalPoly
    | expr '+' expr # union
    | expr '*' expr # intersection
    | '#' expr # boundingBox
    | '!' NUM # random
    | VAR  # var
    | '(' expr ')' # parens
    ;

anytext : (ANY | WS | LINE_COMMENT | SIGN | VAR)*
    ;

numPairSerie: numPair*
    ;

numPair: num num
    ;

num: NUM
    ;

MES : '+' ;
NUM : SIGN? UINT + ('.' [0-9]+)? ;
UINT : [0-9]+ ;
SIGN : ('+'|'-') ;
VAR : [_a-zA-Z0-9]+ ;
WS : [ \t]+ -> skip ;
LINE_COMMENT : '//' ~[\r\n]* -> skip;
NEWLINE : [\n\r] ;
ANY: .;
