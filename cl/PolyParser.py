# Generated from Poly.g by ANTLR 4.9.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3 ")
        buf.write("\u0088\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\3\2\6\2\26\n\2\r\2\16\2\27\3")
        buf.write("\2\5\2\33\n\2\3\2\3\2\3\3\3\3\3\3\3\3\5\3#\n\3\3\4\3\4")
        buf.write("\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3")
        buf.write("\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\6\4K\n\4\r\4\16\4")
        buf.write("L\5\4O\n\4\3\4\3\4\3\4\5\4T\n\4\3\5\3\5\3\5\3\5\3\5\3")
        buf.write("\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6")
        buf.write("\3\6\5\6j\n\6\3\6\3\6\3\6\3\6\3\6\3\6\7\6r\n\6\f\6\16")
        buf.write("\6u\13\6\3\7\7\7x\n\7\f\7\16\7{\13\7\3\b\7\b~\n\b\f\b")
        buf.write("\16\b\u0081\13\b\3\t\3\t\3\t\3\n\3\n\3\n\2\3\n\13\2\4")
        buf.write("\6\b\n\f\16\20\22\2\3\4\2\33\36  \2\u0095\2\25\3\2\2\2")
        buf.write("\4\"\3\2\2\2\6S\3\2\2\2\bU\3\2\2\2\ni\3\2\2\2\fy\3\2\2")
        buf.write("\2\16\177\3\2\2\2\20\u0082\3\2\2\2\22\u0085\3\2\2\2\24")
        buf.write("\26\5\4\3\2\25\24\3\2\2\2\26\27\3\2\2\2\27\25\3\2\2\2")
        buf.write("\27\30\3\2\2\2\30\32\3\2\2\2\31\33\5\6\4\2\32\31\3\2\2")
        buf.write("\2\32\33\3\2\2\2\33\34\3\2\2\2\34\35\7\2\2\3\35\3\3\2")
        buf.write("\2\2\36\37\5\6\4\2\37 \7\37\2\2 #\3\2\2\2!#\7\37\2\2\"")
        buf.write("\36\3\2\2\2\"!\3\2\2\2#\5\3\2\2\2$%\7\3\2\2%T\5\n\6\2")
        buf.write("&\'\7\3\2\2\'(\7\4\2\2()\5\f\7\2)*\7\4\2\2*T\3\2\2\2+")
        buf.write(",\7\5\2\2,T\5\n\6\2-.\7\6\2\2./\5\n\6\2/\60\7\7\2\2\60")
        buf.write("\61\5\b\5\2\61T\3\2\2\2\62\63\7\b\2\2\63T\5\n\6\2\64\65")
        buf.write("\7\t\2\2\65T\5\n\6\2\66\67\7\n\2\2\67T\5\n\6\289\7\13")
        buf.write("\2\29:\5\n\6\2:;\7\7\2\2;<\5\n\6\2<T\3\2\2\2=>\7\f\2\2")
        buf.write(">?\5\n\6\2?@\7\7\2\2@A\5\n\6\2AT\3\2\2\2BC\7\r\2\2CD\7")
        buf.write("\4\2\2DE\5\f\7\2EF\7\4\2\2FG\7\7\2\2GN\5\n\6\2HI\7\7\2")
        buf.write("\2IK\5\n\6\2JH\3\2\2\2KL\3\2\2\2LJ\3\2\2\2LM\3\2\2\2M")
        buf.write("O\3\2\2\2NJ\3\2\2\2NO\3\2\2\2OT\3\2\2\2PQ\7\34\2\2QR\7")
        buf.write("\16\2\2RT\5\n\6\2S$\3\2\2\2S&\3\2\2\2S+\3\2\2\2S-\3\2")
        buf.write("\2\2S\62\3\2\2\2S\64\3\2\2\2S\66\3\2\2\2S8\3\2\2\2S=\3")
        buf.write("\2\2\2SB\3\2\2\2SP\3\2\2\2T\7\3\2\2\2UV\7\17\2\2VW\7\31")
        buf.write("\2\2WX\7\31\2\2XY\7\31\2\2YZ\7\20\2\2Z\t\3\2\2\2[\\\b")
        buf.write("\6\1\2\\]\7\21\2\2]^\5\16\b\2^_\7\22\2\2_j\3\2\2\2`a\7")
        buf.write("\24\2\2aj\5\n\6\6bc\7\25\2\2cj\7\31\2\2dj\7\34\2\2ef\7")
        buf.write("\26\2\2fg\5\n\6\2gh\7\27\2\2hj\3\2\2\2i[\3\2\2\2i`\3\2")
        buf.write("\2\2ib\3\2\2\2id\3\2\2\2ie\3\2\2\2js\3\2\2\2kl\f\b\2\2")
        buf.write("lm\7\30\2\2mr\5\n\6\tno\f\7\2\2op\7\23\2\2pr\5\n\6\bq")
        buf.write("k\3\2\2\2qn\3\2\2\2ru\3\2\2\2sq\3\2\2\2st\3\2\2\2t\13")
        buf.write("\3\2\2\2us\3\2\2\2vx\t\2\2\2wv\3\2\2\2x{\3\2\2\2yw\3\2")
        buf.write("\2\2yz\3\2\2\2z\r\3\2\2\2{y\3\2\2\2|~\5\20\t\2}|\3\2\2")
        buf.write("\2~\u0081\3\2\2\2\177}\3\2\2\2\177\u0080\3\2\2\2\u0080")
        buf.write("\17\3\2\2\2\u0081\177\3\2\2\2\u0082\u0083\5\22\n\2\u0083")
        buf.write("\u0084\5\22\n\2\u0084\21\3\2\2\2\u0085\u0086\7\31\2\2")
        buf.write("\u0086\23\3\2\2\2\r\27\32\"LNSiqsy\177")
        return buf.getvalue()


class PolyParser ( Parser ):

    grammarFileName = "Poly.g"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'print'", "'\"'", "'area'", "'color'", 
                     "','", "'perimeter'", "'vertices'", "'centroid'", "'equal'", 
                     "'inside'", "'draw'", "':='", "'{'", "'}'", "'['", 
                     "']'", "'*'", "'#'", "'!'", "'('", "')'", "'+'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "MES", "NUM", "UINT", "SIGN", 
                      "VAR", "WS", "LINE_COMMENT", "NEWLINE", "ANY" ]

    RULE_root = 0
    RULE_line = 1
    RULE_inst = 2
    RULE_color = 3
    RULE_expr = 4
    RULE_anytext = 5
    RULE_numPairSerie = 6
    RULE_numPair = 7
    RULE_num = 8

    ruleNames =  [ "root", "line", "inst", "color", "expr", "anytext", "numPairSerie", 
                   "numPair", "num" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    MES=22
    NUM=23
    UINT=24
    SIGN=25
    VAR=26
    WS=27
    LINE_COMMENT=28
    NEWLINE=29
    ANY=30

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class RootContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(PolyParser.EOF, 0)

        def line(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(PolyParser.LineContext)
            else:
                return self.getTypedRuleContext(PolyParser.LineContext,i)


        def inst(self):
            return self.getTypedRuleContext(PolyParser.InstContext,0)


        def getRuleIndex(self):
            return PolyParser.RULE_root

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRoot" ):
                return visitor.visitRoot(self)
            else:
                return visitor.visitChildren(self)




    def root(self):

        localctx = PolyParser.RootContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_root)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 19 
            self._errHandler.sync(self)
            _alt = 1
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    self.state = 18
                    self.line()

                else:
                    raise NoViableAltException(self)
                self.state = 21 
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,0,self._ctx)

            self.state = 24
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << PolyParser.T__0) | (1 << PolyParser.T__2) | (1 << PolyParser.T__3) | (1 << PolyParser.T__5) | (1 << PolyParser.T__6) | (1 << PolyParser.T__7) | (1 << PolyParser.T__8) | (1 << PolyParser.T__9) | (1 << PolyParser.T__10) | (1 << PolyParser.VAR))) != 0):
                self.state = 23
                self.inst()


            self.state = 26
            self.match(PolyParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LineContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def inst(self):
            return self.getTypedRuleContext(PolyParser.InstContext,0)


        def NEWLINE(self):
            return self.getToken(PolyParser.NEWLINE, 0)

        def getRuleIndex(self):
            return PolyParser.RULE_line

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLine" ):
                return visitor.visitLine(self)
            else:
                return visitor.visitChildren(self)




    def line(self):

        localctx = PolyParser.LineContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_line)
        try:
            self.state = 32
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [PolyParser.T__0, PolyParser.T__2, PolyParser.T__3, PolyParser.T__5, PolyParser.T__6, PolyParser.T__7, PolyParser.T__8, PolyParser.T__9, PolyParser.T__10, PolyParser.VAR]:
                self.enterOuterAlt(localctx, 1)
                self.state = 28
                self.inst()
                self.state = 29
                self.match(PolyParser.NEWLINE)
                pass
            elif token in [PolyParser.NEWLINE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 31
                self.match(PolyParser.NEWLINE)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InstContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return PolyParser.RULE_inst

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AreaContext(InstContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.InstContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(PolyParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArea" ):
                return visitor.visitArea(self)
            else:
                return visitor.visitChildren(self)


    class EqualContext(InstContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.InstContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(PolyParser.ExprContext)
            else:
                return self.getTypedRuleContext(PolyParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEqual" ):
                return visitor.visitEqual(self)
            else:
                return visitor.visitChildren(self)


    class SetColorContext(InstContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.InstContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(PolyParser.ExprContext,0)

        def color(self):
            return self.getTypedRuleContext(PolyParser.ColorContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSetColor" ):
                return visitor.visitSetColor(self)
            else:
                return visitor.visitChildren(self)


    class VerticesContext(InstContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.InstContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(PolyParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVertices" ):
                return visitor.visitVertices(self)
            else:
                return visitor.visitChildren(self)


    class CentroidContext(InstContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.InstContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(PolyParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCentroid" ):
                return visitor.visitCentroid(self)
            else:
                return visitor.visitChildren(self)


    class PerimeterContext(InstContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.InstContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(PolyParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPerimeter" ):
                return visitor.visitPerimeter(self)
            else:
                return visitor.visitChildren(self)


    class PrintPolyContext(InstContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.InstContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(PolyParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrintPoly" ):
                return visitor.visitPrintPoly(self)
            else:
                return visitor.visitChildren(self)


    class DrawContext(InstContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.InstContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def anytext(self):
            return self.getTypedRuleContext(PolyParser.AnytextContext,0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(PolyParser.ExprContext)
            else:
                return self.getTypedRuleContext(PolyParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDraw" ):
                return visitor.visitDraw(self)
            else:
                return visitor.visitChildren(self)


    class InsideContext(InstContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.InstContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(PolyParser.ExprContext)
            else:
                return self.getTypedRuleContext(PolyParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInside" ):
                return visitor.visitInside(self)
            else:
                return visitor.visitChildren(self)


    class PrintTextContext(InstContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.InstContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def anytext(self):
            return self.getTypedRuleContext(PolyParser.AnytextContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrintText" ):
                return visitor.visitPrintText(self)
            else:
                return visitor.visitChildren(self)


    class AssignContext(InstContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.InstContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def VAR(self):
            return self.getToken(PolyParser.VAR, 0)
        def expr(self):
            return self.getTypedRuleContext(PolyParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssign" ):
                return visitor.visitAssign(self)
            else:
                return visitor.visitChildren(self)



    def inst(self):

        localctx = PolyParser.InstContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_inst)
        self._la = 0 # Token type
        try:
            self.state = 81
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
            if la_ == 1:
                localctx = PolyParser.PrintPolyContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 34
                self.match(PolyParser.T__0)
                self.state = 35
                self.expr(0)
                pass

            elif la_ == 2:
                localctx = PolyParser.PrintTextContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 36
                self.match(PolyParser.T__0)
                self.state = 37
                self.match(PolyParser.T__1)
                self.state = 38
                self.anytext()
                self.state = 39
                self.match(PolyParser.T__1)
                pass

            elif la_ == 3:
                localctx = PolyParser.AreaContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 41
                self.match(PolyParser.T__2)
                self.state = 42
                self.expr(0)
                pass

            elif la_ == 4:
                localctx = PolyParser.SetColorContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 43
                self.match(PolyParser.T__3)
                self.state = 44
                self.expr(0)
                self.state = 45
                self.match(PolyParser.T__4)
                self.state = 46
                self.color()
                pass

            elif la_ == 5:
                localctx = PolyParser.PerimeterContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 48
                self.match(PolyParser.T__5)
                self.state = 49
                self.expr(0)
                pass

            elif la_ == 6:
                localctx = PolyParser.VerticesContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 50
                self.match(PolyParser.T__6)
                self.state = 51
                self.expr(0)
                pass

            elif la_ == 7:
                localctx = PolyParser.CentroidContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 52
                self.match(PolyParser.T__7)
                self.state = 53
                self.expr(0)
                pass

            elif la_ == 8:
                localctx = PolyParser.EqualContext(self, localctx)
                self.enterOuterAlt(localctx, 8)
                self.state = 54
                self.match(PolyParser.T__8)
                self.state = 55
                self.expr(0)
                self.state = 56
                self.match(PolyParser.T__4)
                self.state = 57
                self.expr(0)
                pass

            elif la_ == 9:
                localctx = PolyParser.InsideContext(self, localctx)
                self.enterOuterAlt(localctx, 9)
                self.state = 59
                self.match(PolyParser.T__9)
                self.state = 60
                self.expr(0)
                self.state = 61
                self.match(PolyParser.T__4)
                self.state = 62
                self.expr(0)
                pass

            elif la_ == 10:
                localctx = PolyParser.DrawContext(self, localctx)
                self.enterOuterAlt(localctx, 10)
                self.state = 64
                self.match(PolyParser.T__10)
                self.state = 65
                self.match(PolyParser.T__1)
                self.state = 66
                self.anytext()
                self.state = 67
                self.match(PolyParser.T__1)
                self.state = 68
                self.match(PolyParser.T__4)
                self.state = 69
                self.expr(0)
                self.state = 76
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==PolyParser.T__4:
                    self.state = 72 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while True:
                        self.state = 70
                        self.match(PolyParser.T__4)
                        self.state = 71
                        self.expr(0)
                        self.state = 74 
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        if not (_la==PolyParser.T__4):
                            break



                pass

            elif la_ == 11:
                localctx = PolyParser.AssignContext(self, localctx)
                self.enterOuterAlt(localctx, 11)
                self.state = 78
                self.match(PolyParser.VAR)
                self.state = 79
                self.match(PolyParser.T__11)
                self.state = 80
                self.expr(0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ColorContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUM(self, i:int=None):
            if i is None:
                return self.getTokens(PolyParser.NUM)
            else:
                return self.getToken(PolyParser.NUM, i)

        def getRuleIndex(self):
            return PolyParser.RULE_color

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitColor" ):
                return visitor.visitColor(self)
            else:
                return visitor.visitChildren(self)




    def color(self):

        localctx = PolyParser.ColorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_color)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 83
            self.match(PolyParser.T__12)
            self.state = 84
            self.match(PolyParser.NUM)
            self.state = 85
            self.match(PolyParser.NUM)
            self.state = 86
            self.match(PolyParser.NUM)
            self.state = 87
            self.match(PolyParser.T__13)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExprContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return PolyParser.RULE_expr

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class BoundingBoxContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(PolyParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBoundingBox" ):
                return visitor.visitBoundingBox(self)
            else:
                return visitor.visitChildren(self)


    class RandomContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NUM(self):
            return self.getToken(PolyParser.NUM, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRandom" ):
                return visitor.visitRandom(self)
            else:
                return visitor.visitChildren(self)


    class ParensContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(PolyParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParens" ):
                return visitor.visitParens(self)
            else:
                return visitor.visitChildren(self)


    class LiteralPolyContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def numPairSerie(self):
            return self.getTypedRuleContext(PolyParser.NumPairSerieContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLiteralPoly" ):
                return visitor.visitLiteralPoly(self)
            else:
                return visitor.visitChildren(self)


    class VarContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def VAR(self):
            return self.getToken(PolyParser.VAR, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVar" ):
                return visitor.visitVar(self)
            else:
                return visitor.visitChildren(self)


    class IntersectionContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(PolyParser.ExprContext)
            else:
                return self.getTypedRuleContext(PolyParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIntersection" ):
                return visitor.visitIntersection(self)
            else:
                return visitor.visitChildren(self)


    class UnionContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a PolyParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(PolyParser.ExprContext)
            else:
                return self.getTypedRuleContext(PolyParser.ExprContext,i)

        def MES(self):
            return self.getToken(PolyParser.MES, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnion" ):
                return visitor.visitUnion(self)
            else:
                return visitor.visitChildren(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = PolyParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 8
        self.enterRecursionRule(localctx, 8, self.RULE_expr, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 103
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [PolyParser.T__14]:
                localctx = PolyParser.LiteralPolyContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 90
                self.match(PolyParser.T__14)
                self.state = 91
                self.numPairSerie()
                self.state = 92
                self.match(PolyParser.T__15)
                pass
            elif token in [PolyParser.T__17]:
                localctx = PolyParser.BoundingBoxContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 94
                self.match(PolyParser.T__17)
                self.state = 95
                self.expr(4)
                pass
            elif token in [PolyParser.T__18]:
                localctx = PolyParser.RandomContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 96
                self.match(PolyParser.T__18)
                self.state = 97
                self.match(PolyParser.NUM)
                pass
            elif token in [PolyParser.VAR]:
                localctx = PolyParser.VarContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 98
                self.match(PolyParser.VAR)
                pass
            elif token in [PolyParser.T__19]:
                localctx = PolyParser.ParensContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 99
                self.match(PolyParser.T__19)
                self.state = 100
                self.expr(0)
                self.state = 101
                self.match(PolyParser.T__20)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 113
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,8,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 111
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,7,self._ctx)
                    if la_ == 1:
                        localctx = PolyParser.UnionContext(self, PolyParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 105
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 106
                        self.match(PolyParser.MES)
                        self.state = 107
                        self.expr(7)
                        pass

                    elif la_ == 2:
                        localctx = PolyParser.IntersectionContext(self, PolyParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 108
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 109
                        self.match(PolyParser.T__16)
                        self.state = 110
                        self.expr(6)
                        pass

             
                self.state = 115
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,8,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class AnytextContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ANY(self, i:int=None):
            if i is None:
                return self.getTokens(PolyParser.ANY)
            else:
                return self.getToken(PolyParser.ANY, i)

        def WS(self, i:int=None):
            if i is None:
                return self.getTokens(PolyParser.WS)
            else:
                return self.getToken(PolyParser.WS, i)

        def LINE_COMMENT(self, i:int=None):
            if i is None:
                return self.getTokens(PolyParser.LINE_COMMENT)
            else:
                return self.getToken(PolyParser.LINE_COMMENT, i)

        def SIGN(self, i:int=None):
            if i is None:
                return self.getTokens(PolyParser.SIGN)
            else:
                return self.getToken(PolyParser.SIGN, i)

        def VAR(self, i:int=None):
            if i is None:
                return self.getTokens(PolyParser.VAR)
            else:
                return self.getToken(PolyParser.VAR, i)

        def getRuleIndex(self):
            return PolyParser.RULE_anytext

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAnytext" ):
                return visitor.visitAnytext(self)
            else:
                return visitor.visitChildren(self)




    def anytext(self):

        localctx = PolyParser.AnytextContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_anytext)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 119
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << PolyParser.SIGN) | (1 << PolyParser.VAR) | (1 << PolyParser.WS) | (1 << PolyParser.LINE_COMMENT) | (1 << PolyParser.ANY))) != 0):
                self.state = 116
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << PolyParser.SIGN) | (1 << PolyParser.VAR) | (1 << PolyParser.WS) | (1 << PolyParser.LINE_COMMENT) | (1 << PolyParser.ANY))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 121
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NumPairSerieContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def numPair(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(PolyParser.NumPairContext)
            else:
                return self.getTypedRuleContext(PolyParser.NumPairContext,i)


        def getRuleIndex(self):
            return PolyParser.RULE_numPairSerie

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNumPairSerie" ):
                return visitor.visitNumPairSerie(self)
            else:
                return visitor.visitChildren(self)




    def numPairSerie(self):

        localctx = PolyParser.NumPairSerieContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_numPairSerie)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 125
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==PolyParser.NUM:
                self.state = 122
                self.numPair()
                self.state = 127
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NumPairContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def num(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(PolyParser.NumContext)
            else:
                return self.getTypedRuleContext(PolyParser.NumContext,i)


        def getRuleIndex(self):
            return PolyParser.RULE_numPair

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNumPair" ):
                return visitor.visitNumPair(self)
            else:
                return visitor.visitChildren(self)




    def numPair(self):

        localctx = PolyParser.NumPairContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_numPair)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 128
            self.num()
            self.state = 129
            self.num()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NumContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUM(self):
            return self.getToken(PolyParser.NUM, 0)

        def getRuleIndex(self):
            return PolyParser.RULE_num

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNum" ):
                return visitor.visitNum(self)
            else:
                return visitor.visitChildren(self)




    def num(self):

        localctx = PolyParser.NumContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_num)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 131
            self.match(PolyParser.NUM)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[4] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 5)
         




