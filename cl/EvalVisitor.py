if __name__ is not None and "." in __name__:
    from .PolyParser import PolyParser
    from .PolyVisitor import PolyVisitor
else:
    from PolyParser import PolyParser
    from PolyVisitor import PolyVisitor
import sys

sys.path.append("..")
from polygons import ConvexPolygon
from polygons import draw_convex_polygons
import random


class EvalVisitor(PolyVisitor):

    def __init__(self, telegram=False):
        self.memory = {}
        self.output = []
        self.telegram = telegram
        if telegram:
            self.output_list = []
            self.image_list = []

    def __bool_to_string(self, b):
        if b:
            return 'yes'
        else:
            return 'no'

    def __print_to_file(self, p):
        if self.telegram:
            self.output_list.append(str(p) + '\n')
        else:
            print(p)

    def __image_saved(self, im):
        if self.telegram:
            self.image_list.append(im)

    def visitAssign(self, ctx: PolyParser.AssignContext):
        l = [n for n in ctx.getChildren()]
        name = l[0].getText()
        self.memory[name] = self.visit(l[2])

    def visitPrintPoly(self, ctx: PolyParser.PrintPolyContext):
        l = [n for n in ctx.getChildren()]
        self.__print_to_file(self.visit(l[1]).get_vertices())

    def visitPrintText(self, ctx: PolyParser.PrintTextContext):
        l = [n for n in ctx.getChildren()]
        self.__print_to_file(self.visit(l[2]))

    def visitAnytext(self, ctx: PolyParser.AnytextContext):
        return ''.join([n.getText() for n in ctx.getChildren()])

    def visitArea(self, ctx: PolyParser.AreaContext):
        l = [n for n in ctx.getChildren()]
        self.__print_to_file(self.visit(l[1]).get_area())

    def visitSetColor(self, ctx: PolyParser.SetColorContext):
        l = [n for n in ctx.getChildren()]
        self.visit(l[1]).set_color(self.visit(l[3]))

    def visitPerimeter(self, ctx: PolyParser.PerimeterContext):
        l = [n for n in ctx.getChildren()]
        self.__print_to_file(self.visit(l[1]).get_perimeter())

    def visitVertices(self, ctx: PolyParser.VerticesContext):
        l = [n for n in ctx.getChildren()]
        self.__print_to_file(self.visit(l[1]).get_num_edges_and_verts())

    def visitCentroid(self, ctx: PolyParser.CentroidContext):
        l = [n for n in ctx.getChildren()]
        self.__print_to_file(self.visit(l[1]).get_centroid())

    def visitEqual(self, ctx: PolyParser.EqualContext):
        l = [n for n in ctx.getChildren()]
        self.__print_to_file(self.__bool_to_string(self.visit(l[1]).equals(self.visit(l[3]))))

    def visitInside(self, ctx: PolyParser.InsideContext):
        l = [n for n in ctx.getChildren()]
        self.__print_to_file(self.__bool_to_string(self.visit(l[1]).poly_inside_polygon(self.visit(l[3]))))

    def visitDraw(self, ctx: PolyParser.DrawContext):
        l = [n for n in ctx.getChildren()]
        polys = [self.visit(n) for n in l[5::2]]
        image_name = self.visit(l[2])
        self.__image_saved(image_name)
        draw_convex_polygons(polys, image_name)

    def visitUnion(self, ctx: PolyParser.UnionContext):
        l = [n for n in ctx.getChildren()]
        return self.visit(l[0]).convex_union(self.visit(l[2]))

    def visitIntersection(self, ctx: PolyParser.IntersectionContext):
        l = [n for n in ctx.getChildren()]
        return self.visit(l[0]).intersection(self.visit(l[2]))

    def visitBoundingBox(self, ctx: PolyParser.BoundingBoxContext):
        l = [n for n in ctx.getChildren()]
        return self.visit(l[1]).get_bounding_box()

    def visitRandom(self, ctx: PolyParser.RandomContext):
        l = [n for n in ctx.getChildren()]
        n_points = int(l[1].getText())
        points = [[random.uniform(0, 1), random.uniform(0, 1)] for i in range(n_points)]
        return ConvexPolygon(points)

    def visitParens(self, ctx: PolyParser.ParensContext):
        l = [n for n in ctx.getChildren()]
        return self.visit(l[1])

    def visitLiteralPoly(self, ctx: PolyParser.LiteralPolyContext):
        l = [n for n in ctx.getChildren()]
        return ConvexPolygon(self.visit(l[1]))

    def visitVar(self, ctx: PolyParser.VarContext):
        l = [n for n in ctx.getChildren()]
        return self.memory[l[0].getText()]

    def visitNumPairSerie(self, ctx: PolyParser.NumPairSerieContext):
        points = []
        for n in ctx.getChildren():
            points.append(self.visit(n))
        return points

    def visitNumPair(self, ctx: PolyParser.NumPairContext):
        point = []
        for n in ctx.getChildren():
            point.append(self.visit(n))
        return point

    def visitColor(self, ctx: PolyParser.ColorContext):
        l = [n for n in ctx.getChildren()]
        return tuple(255 * int(c.getText()) for c in l[1:4])

    def visitNum(self, ctx: PolyParser.NumContext):
        return float(ctx.NUM().getText())
