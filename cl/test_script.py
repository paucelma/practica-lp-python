import sys
from antlr4 import *
from PolyLexer import PolyLexer
from PolyParser import PolyParser
from EvalVisitor import EvalVisitor
from polygons import ConvexPolygon

input_stream = FileStream('script.poly')
lexer = PolyLexer(input_stream)
token_stream = CommonTokenStream(lexer)
parser = PolyParser(token_stream)

tree = parser.root()
visitor = EvalVisitor()
visitor.visit(tree)
