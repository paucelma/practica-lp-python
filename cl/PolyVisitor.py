# Generated from Poly.g by ANTLR 4.9.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .PolyParser import PolyParser
else:
    from PolyParser import PolyParser

# This class defines a complete generic visitor for a parse tree produced by PolyParser.

class PolyVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by PolyParser#root.
    def visitRoot(self, ctx:PolyParser.RootContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#line.
    def visitLine(self, ctx:PolyParser.LineContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#printPoly.
    def visitPrintPoly(self, ctx:PolyParser.PrintPolyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#printText.
    def visitPrintText(self, ctx:PolyParser.PrintTextContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#area.
    def visitArea(self, ctx:PolyParser.AreaContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#setColor.
    def visitSetColor(self, ctx:PolyParser.SetColorContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#perimeter.
    def visitPerimeter(self, ctx:PolyParser.PerimeterContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#vertices.
    def visitVertices(self, ctx:PolyParser.VerticesContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#centroid.
    def visitCentroid(self, ctx:PolyParser.CentroidContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#equal.
    def visitEqual(self, ctx:PolyParser.EqualContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#inside.
    def visitInside(self, ctx:PolyParser.InsideContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#draw.
    def visitDraw(self, ctx:PolyParser.DrawContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#assign.
    def visitAssign(self, ctx:PolyParser.AssignContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#color.
    def visitColor(self, ctx:PolyParser.ColorContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#boundingBox.
    def visitBoundingBox(self, ctx:PolyParser.BoundingBoxContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#random.
    def visitRandom(self, ctx:PolyParser.RandomContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#parens.
    def visitParens(self, ctx:PolyParser.ParensContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#literalPoly.
    def visitLiteralPoly(self, ctx:PolyParser.LiteralPolyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#var.
    def visitVar(self, ctx:PolyParser.VarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#intersection.
    def visitIntersection(self, ctx:PolyParser.IntersectionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#union.
    def visitUnion(self, ctx:PolyParser.UnionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#anytext.
    def visitAnytext(self, ctx:PolyParser.AnytextContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#numPairSerie.
    def visitNumPairSerie(self, ctx:PolyParser.NumPairSerieContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#numPair.
    def visitNumPair(self, ctx:PolyParser.NumPairContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PolyParser#num.
    def visitNum(self, ctx:PolyParser.NumContext):
        return self.visitChildren(ctx)



del PolyParser