# Generated from Poly.g by ANTLR 4.9.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2 ")
        buf.write("\u00cb\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3")
        buf.write("\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\7\3\7")
        buf.write("\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3")
        buf.write("\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n")
        buf.write("\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13")
        buf.write("\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\17\3\17\3")
        buf.write("\20\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25")
        buf.write("\3\25\3\26\3\26\3\27\3\27\3\30\5\30\u009b\n\30\3\30\6")
        buf.write("\30\u009e\n\30\r\30\16\30\u009f\3\30\3\30\6\30\u00a4\n")
        buf.write("\30\r\30\16\30\u00a5\5\30\u00a8\n\30\3\31\6\31\u00ab\n")
        buf.write("\31\r\31\16\31\u00ac\3\32\3\32\3\33\6\33\u00b2\n\33\r")
        buf.write("\33\16\33\u00b3\3\34\6\34\u00b7\n\34\r\34\16\34\u00b8")
        buf.write("\3\34\3\34\3\35\3\35\3\35\3\35\7\35\u00c1\n\35\f\35\16")
        buf.write("\35\u00c4\13\35\3\35\3\35\3\36\3\36\3\37\3\37\2\2 \3\3")
        buf.write("\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16")
        buf.write("\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61")
        buf.write("\32\63\33\65\34\67\359\36;\37= \3\2\7\3\2\62;\4\2--//")
        buf.write("\6\2\62;C\\aac|\4\2\13\13\"\"\4\2\f\f\17\17\2\u00d2\2")
        buf.write("\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3")
        buf.write("\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2")
        buf.write("\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2")
        buf.write("\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%")
        buf.write("\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2")
        buf.write("\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67")
        buf.write("\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\3?\3\2\2\2\5")
        buf.write("E\3\2\2\2\7G\3\2\2\2\tL\3\2\2\2\13R\3\2\2\2\rT\3\2\2\2")
        buf.write("\17^\3\2\2\2\21g\3\2\2\2\23p\3\2\2\2\25v\3\2\2\2\27}\3")
        buf.write("\2\2\2\31\u0082\3\2\2\2\33\u0085\3\2\2\2\35\u0087\3\2")
        buf.write("\2\2\37\u0089\3\2\2\2!\u008b\3\2\2\2#\u008d\3\2\2\2%\u008f")
        buf.write("\3\2\2\2\'\u0091\3\2\2\2)\u0093\3\2\2\2+\u0095\3\2\2\2")
        buf.write("-\u0097\3\2\2\2/\u009a\3\2\2\2\61\u00aa\3\2\2\2\63\u00ae")
        buf.write("\3\2\2\2\65\u00b1\3\2\2\2\67\u00b6\3\2\2\29\u00bc\3\2")
        buf.write("\2\2;\u00c7\3\2\2\2=\u00c9\3\2\2\2?@\7r\2\2@A\7t\2\2A")
        buf.write("B\7k\2\2BC\7p\2\2CD\7v\2\2D\4\3\2\2\2EF\7$\2\2F\6\3\2")
        buf.write("\2\2GH\7c\2\2HI\7t\2\2IJ\7g\2\2JK\7c\2\2K\b\3\2\2\2LM")
        buf.write("\7e\2\2MN\7q\2\2NO\7n\2\2OP\7q\2\2PQ\7t\2\2Q\n\3\2\2\2")
        buf.write("RS\7.\2\2S\f\3\2\2\2TU\7r\2\2UV\7g\2\2VW\7t\2\2WX\7k\2")
        buf.write("\2XY\7o\2\2YZ\7g\2\2Z[\7v\2\2[\\\7g\2\2\\]\7t\2\2]\16")
        buf.write("\3\2\2\2^_\7x\2\2_`\7g\2\2`a\7t\2\2ab\7v\2\2bc\7k\2\2")
        buf.write("cd\7e\2\2de\7g\2\2ef\7u\2\2f\20\3\2\2\2gh\7e\2\2hi\7g")
        buf.write("\2\2ij\7p\2\2jk\7v\2\2kl\7t\2\2lm\7q\2\2mn\7k\2\2no\7")
        buf.write("f\2\2o\22\3\2\2\2pq\7g\2\2qr\7s\2\2rs\7w\2\2st\7c\2\2")
        buf.write("tu\7n\2\2u\24\3\2\2\2vw\7k\2\2wx\7p\2\2xy\7u\2\2yz\7k")
        buf.write("\2\2z{\7f\2\2{|\7g\2\2|\26\3\2\2\2}~\7f\2\2~\177\7t\2")
        buf.write("\2\177\u0080\7c\2\2\u0080\u0081\7y\2\2\u0081\30\3\2\2")
        buf.write("\2\u0082\u0083\7<\2\2\u0083\u0084\7?\2\2\u0084\32\3\2")
        buf.write("\2\2\u0085\u0086\7}\2\2\u0086\34\3\2\2\2\u0087\u0088\7")
        buf.write("\177\2\2\u0088\36\3\2\2\2\u0089\u008a\7]\2\2\u008a \3")
        buf.write("\2\2\2\u008b\u008c\7_\2\2\u008c\"\3\2\2\2\u008d\u008e")
        buf.write("\7,\2\2\u008e$\3\2\2\2\u008f\u0090\7%\2\2\u0090&\3\2\2")
        buf.write("\2\u0091\u0092\7#\2\2\u0092(\3\2\2\2\u0093\u0094\7*\2")
        buf.write("\2\u0094*\3\2\2\2\u0095\u0096\7+\2\2\u0096,\3\2\2\2\u0097")
        buf.write("\u0098\7-\2\2\u0098.\3\2\2\2\u0099\u009b\5\63\32\2\u009a")
        buf.write("\u0099\3\2\2\2\u009a\u009b\3\2\2\2\u009b\u009d\3\2\2\2")
        buf.write("\u009c\u009e\5\61\31\2\u009d\u009c\3\2\2\2\u009e\u009f")
        buf.write("\3\2\2\2\u009f\u009d\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0")
        buf.write("\u00a7\3\2\2\2\u00a1\u00a3\7\60\2\2\u00a2\u00a4\t\2\2")
        buf.write("\2\u00a3\u00a2\3\2\2\2\u00a4\u00a5\3\2\2\2\u00a5\u00a3")
        buf.write("\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6\u00a8\3\2\2\2\u00a7")
        buf.write("\u00a1\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\60\3\2\2\2\u00a9")
        buf.write("\u00ab\t\2\2\2\u00aa\u00a9\3\2\2\2\u00ab\u00ac\3\2\2\2")
        buf.write("\u00ac\u00aa\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad\62\3\2")
        buf.write("\2\2\u00ae\u00af\t\3\2\2\u00af\64\3\2\2\2\u00b0\u00b2")
        buf.write("\t\4\2\2\u00b1\u00b0\3\2\2\2\u00b2\u00b3\3\2\2\2\u00b3")
        buf.write("\u00b1\3\2\2\2\u00b3\u00b4\3\2\2\2\u00b4\66\3\2\2\2\u00b5")
        buf.write("\u00b7\t\5\2\2\u00b6\u00b5\3\2\2\2\u00b7\u00b8\3\2\2\2")
        buf.write("\u00b8\u00b6\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9\u00ba\3")
        buf.write("\2\2\2\u00ba\u00bb\b\34\2\2\u00bb8\3\2\2\2\u00bc\u00bd")
        buf.write("\7\61\2\2\u00bd\u00be\7\61\2\2\u00be\u00c2\3\2\2\2\u00bf")
        buf.write("\u00c1\n\6\2\2\u00c0\u00bf\3\2\2\2\u00c1\u00c4\3\2\2\2")
        buf.write("\u00c2\u00c0\3\2\2\2\u00c2\u00c3\3\2\2\2\u00c3\u00c5\3")
        buf.write("\2\2\2\u00c4\u00c2\3\2\2\2\u00c5\u00c6\b\35\2\2\u00c6")
        buf.write(":\3\2\2\2\u00c7\u00c8\t\6\2\2\u00c8<\3\2\2\2\u00c9\u00ca")
        buf.write("\13\2\2\2\u00ca>\3\2\2\2\13\2\u009a\u009f\u00a5\u00a7")
        buf.write("\u00ac\u00b3\u00b8\u00c2\3\b\2\2")
        return buf.getvalue()


class PolyLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    T__15 = 16
    T__16 = 17
    T__17 = 18
    T__18 = 19
    T__19 = 20
    T__20 = 21
    MES = 22
    NUM = 23
    UINT = 24
    SIGN = 25
    VAR = 26
    WS = 27
    LINE_COMMENT = 28
    NEWLINE = 29
    ANY = 30

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'print'", "'\"'", "'area'", "'color'", "','", "'perimeter'", 
            "'vertices'", "'centroid'", "'equal'", "'inside'", "'draw'", 
            "':='", "'{'", "'}'", "'['", "']'", "'*'", "'#'", "'!'", "'('", 
            "')'", "'+'" ]

    symbolicNames = [ "<INVALID>",
            "MES", "NUM", "UINT", "SIGN", "VAR", "WS", "LINE_COMMENT", "NEWLINE", 
            "ANY" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "T__13", 
                  "T__14", "T__15", "T__16", "T__17", "T__18", "T__19", 
                  "T__20", "MES", "NUM", "UINT", "SIGN", "VAR", "WS", "LINE_COMMENT", 
                  "NEWLINE", "ANY" ]

    grammarFileName = "Poly.g"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


