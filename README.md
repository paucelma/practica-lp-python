# Pràctica de Python - LP Q1 20/21: PolyBot

## Pasos previs a l'execució

Per executar aquesta pràctica, caldrà tenir instal·lat Python3, pip i la versió 4.9.1 d'ANTLR. A més, caldrà executar la 
comanda`pip install -r requirements.txt` per instal·lar els paquets de Python dels que depèn el codi de la pràctica.

## Part 1: Convex Polygons
### Descripció
La primera part de la pràctica consisteix d'un mòdul polygons que ens permet definir i fer operacions amb polígons 
convexes. Disposa d'un script de test `test_polygons.py` que permet testejar totes les funcions d'aquesta part en varis 
casos de prova.

### Estructura bàsica
El mòdul està format per:
* La funció estàtica *draw_convex_polygons*, que permet dibuixar els polígons que se li passin per paràmetre.
* La classe ConvexPolygons, que té les següents funcionalitats implementades:
    - Una constructura que permet definir un polígon a partir d'una sèrie de punts.
    - Una funció *point_inside_polygon* que permet saber si un punt es troba dins un polígon.
    - Una funció *poly_inside_polygon* que permet saber si un polígon es troba dins un altre.
    - Una funció *get_perimeter* que retorna el perímetre d'un polígon.
    - Una funció *get_area* que retorna l'àrea d'un polígon.
    - Una funció *get_centroid* que retorna el centroide d'un polígon.
    - Una funció *is_equilateral* que permet saber si un polígon és equilateral.
    - Una funció *is_equiangular* que permet saber si un polígon és equiangular.
    - Una funció *is_regular* que permet saber si un polígon és regular.
    - Una funció *get_bounding_box* que retorna la *bounding box* d'un polígon.
    - Una funció *intersection* que retorna la intersecció d'un polígon amb un altre.
    - Una funció *convex_union* que retorna la unió convexa d'un polígon amb un altre.
    - Una funció *equals* que retorna si un polígon és igual a un altre.
    
Aquestes funcions i els operadors privats de la classe estan degudament documentades al mateix codi.

## Part 2: A programming language to work with convex polygons
### Descripció
La segona part de la pràctica, que es troba a la carpeta `cl`, consisteix en una gramàtica ANTLR4 i un *Visitor* que 
ens permeten traduïr a Python un petit llenguatge de programació que hem inventat per operar amb polígons. 
Disposa d'un script de test `test_script.py` que compila i executa el codi que hi ha a `script.poly`.

### Estructura bàsica
Aquesta part està formada per:
* Una gramàtica `Poly.g` que representa el nostre llenguatge de programació.
* Un *Visitor* `EvalVisitor.py` que visitarà l'arbre resultant de passar un script per la nostra gramàtica.
* Arxius resultants de compilar la gramàtica: un *Lexer*, un *Parser*, un *Visitor*...

### Pasos previs a l'execució
Si es vol, es pot tornar a compilar la gramàtica amb la comanda `antlr4 -Dlanguage=Python3 -no-listener -visitor Poly.g`

## Part 3: A bot to interact with convex polygons
### Descripció
La tercera part de la pràctica consisteix en un bot de Telegram que ens permet operar amb polígons convexes des del mòbil
utilitzant el llenguatge de programació que hem creat a la segona part. 

### Estructura bàsica
El bot està format per:
* Un script principal `bot.py` que haurem d'executar per posar en marxa el bot.
* Una interfície `CompilerInterface.py` que ens permet compilar el text del missatge que ens enviï l'usuari i
extreure els resultats de l'execució.
* Un arxiu `token.txt` **NO** inclós al lliurament que conté el id de token del nostre bot de Telegram. Per aconseguir-ne
un caldrà seguir les instruccions que es donen al [material docent de l'assignatura](https://lliçons.jutge.org/python/telegram.html).

