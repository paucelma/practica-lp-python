from polygons import ConvexPolygon
from polygons import draw_convex_polygons

if __name__ == '__main__':
    # Constructora
    poly = ConvexPolygon([(-2, 0), (1.5, -1.5), (0, -2), (-1.5, 1.5), (2, 0), (1.5, 1.5), (0, 2), (-1.5, -1.5)])
    poly2 = ConvexPolygon([(-1.0, -1.0), (1.0, 1.0), (-1.0, 1.0), (1.0, -1.0)])
    poly3 = ConvexPolygon([(-4.0, -4.0), (2.0, 2.0), (-4.0, 2.0), (2.0, -4.0)])
    poly4 = ConvexPolygon([(-0.1, -0.1), (0.1, 0.1), (-0.1, 0.1), (0.1, -0.1), (1.0, 0.0)])
    poly5 = ConvexPolygon([(-2, 0), (1.5, -1.5), (0, -2), (-1.5, 1.5), (2, 0), (1.5, 1.5), (0, 2), (-1.5, -1.5)])
    poly6 = ConvexPolygon([])
    poly7 = ConvexPolygon([(1, 0)])
    poly8 = ConvexPolygon([(1, 0), (0, 1)])

    # Point inside polygon
    print("Point inside polygon")
    print(poly.point_inside_polygon((0, 0)))
    print(poly.point_inside_polygon((2.5, 2.5)))
    print(poly.point_inside_polygon((-2.0, -2.0)))
    print(poly.point_inside_polygon((-2, 0)))
    print(poly2.point_inside_polygon((-1.0, 0)))
    print()

    # Poly inside polygon
    print("Polygon inside polygon")
    print(poly.poly_inside_polygon(poly2))
    print(poly3.poly_inside_polygon(poly))
    print(poly.poly_inside_polygon(poly4))
    print(poly4.poly_inside_polygon(poly))
    print()

    # Perimeter
    print("Perimeter")
    print(poly.get_perimeter())
    print(poly2.get_perimeter())
    print(poly3.get_perimeter())
    print(poly4.get_perimeter())
    print()

    # Area
    print("Area")
    print(poly.get_signed_area())
    print(poly.get_area())
    print(poly2.get_area())
    print(poly3.get_area())
    print(poly4.get_area())
    print()

    # Centroid
    print("Centroid")
    print(poly.get_centroid())
    print(poly2.get_centroid())
    print(poly3.get_centroid())
    print(poly4.get_centroid())
    print()

    # Is equilateral
    print("Is equilateral")
    print(poly.is_equilateral())
    print(poly2.is_equilateral())
    print(poly3.is_equilateral())
    print(poly4.is_equilateral())
    print()

    # Is equiangular
    print("Is equiangular")
    print(poly.is_equiangular())
    print(poly2.is_equiangular())
    print(poly3.is_equiangular())
    print(poly4.is_equiangular())
    print()

    # Is regular
    print("Is regular")
    print(poly.is_regular())
    print(poly2.is_regular())
    print(poly3.is_regular())
    print(poly4.is_regular())
    print()

    # Bounding box
    print("Bounding box")
    print(poly.get_bounding_box().get_vertices())
    print(poly2.get_bounding_box().get_vertices())
    print(poly3.get_bounding_box().get_vertices())
    print(poly4.get_bounding_box().get_vertices())
    print()

    # Intersection
    print("Intersection")
    print(poly2.intersection(poly3).get_vertices())
    print(poly3.intersection(poly2).get_vertices())
    print(poly3.intersection(poly4).get_vertices())
    print(poly4.intersection(poly3).get_vertices())
    print()

    # Convex union
    print("Convex union")
    print(poly3.convex_union(poly4).get_vertices())
    print(poly.convex_union(poly3).get_vertices())
    print()

    # Equals
    print("Equals")
    print(poly5.equals(poly))
    print(poly.equals(poly5))
    print(poly3.equals(poly))
    print()

    # Draw
    # draw_convex_polygons([ConvexPolygon([(0.5, 0.5), (0.0, 1.0), (1.0, 1.0)]), ConvexPolygon([(0.0, 0.0), (1.0, 1.0), (1.0, 0.0)])])
    p = ConvexPolygon([(0, 0.5), (0.5, 0.51), (1, 0.5), (0.5, 0)])
    p.set_color((255, 0, 0))
    print(p.get_color())
    draw_convex_polygons([p])
